Simple HTTP server returning calendar entries for all [Wekan](https://github.com/wekan/wekan) cards with set due date. To be used with Lightning or similar calendar app (read-only).
Yes, it's not the best solution, but it does the job (at least for me).
Depends on: https://github.com/wekan/wekan-python-api-client.

This is a fork of https://github.com/stlehmann/wekan-ical-server. 
It adds functionality that it can work in a multiuser env, and seperate iCals for different boards.

Configuration:
Edit the docker-compose.yml file environment section for configuration:

* `WEKAN_HOST`: Hostname of your wekan server
* `LISTEN_HOST`: Hostname of the ical server (default: 0.0.0.0)
* `LISTEN_PORT`: Port of the ical server (default: 8091)

Running:
Start the server with the following command:

```bash
$ docker-compose up
```

get the iCal link with the following urls: 
* `http://<host>:<port>/ical?user=<username>&pass=<password>` get the cards from all boards with a due date
* `http://<host>:<port>/ical?user=<username>&pass=<password>&board=<board-id>` get the cards from a specific boards with a due date

