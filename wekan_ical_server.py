import os
from wekanapi import WekanApi
from http.server import BaseHTTPRequestHandler, HTTPServer
import vobject
import dateutil.parser


LISTEN_HOST = os.environ.get("LISTEN_HOST", default="")
LISTEN_PORT = int(os.environ.get("LISTEN_PORT", default=8091))
WEKAN_HOST = os.environ["WEKAN_HOST"]


def create_ical_event(cal, board, card, card_info):
    event = cal.add("vevent")
    event.add("summary").value = board.title + ": " + card_info["title"]
    event.add("dtstart").value = dateutil.parser.parse(card_info["dueAt"])
    if "description" in card_info:
        event.add("description").value = card_info["description"]
    event.add("url").value = WEKAN_HOST + "/b/" + board.id + "/x/" + card.id


class MyHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        if not self.path.startswith("/ical?"):
            self.send_error(404)
            return

        parts = self.path.split("?")[1].split("&")

        user = ""
        password = ""
        board_id = ""

        for x in parts:
            name,value = x.split("=")
            if name == "user":
                user = value
            if name == "pass":
                password = value
            if name == "board":
                board_id = value

        wekan_api = WekanApi(
            WEKAN_HOST, {"username": user, "password": password}
        )

        cal = vobject.iCalendar()
        boards = wekan_api.get_user_boards()
        for board in boards:
            if board_id == "" or board.id == board_id:
                cards_lists = board.get_cardslists()
                for cardsList in cards_lists:
                    cards = cardsList.get_cards()
                    for card in cards:
                        info = card.get_card_info()
                        if "dueAt" in info and info["dueAt"] is not None:
                            create_ical_event(cal, board, card, info)

        self.send_response(200)
        # self.send_header("Content-type", "text/calendar")
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(cal.serialize().encode())


if __name__ == "__main__":
    httpd = HTTPServer((LISTEN_HOST, LISTEN_PORT), MyHandler)
    try:
        httpd.serve_forever()
    finally:
        httpd.server_close()
